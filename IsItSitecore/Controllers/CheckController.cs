﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;
using IsItSitecore.DTO;
using IsItSitecore.ViewModels;

namespace IsItSitecore.Controllers
{
    public class CheckController : ApiController
    {
        public Resource[] Resources = {
            new Resource("/webedit.css","text/css"),
            new Resource("/sitecore/no.css","text/css"),
            new Resource("/~/media/System/Template%20Thumbnails/audio.ashx","image/png"),
            new Resource("/~/media/System/Simulator%20Backgrounds/blackberry.ashx","image/png"),
            new Resource("/layouts/System/VisitorIdentification.aspx","text/css"),
        };

        // GET api/values
        public IEnumerable<string> Get()
        {
            return new[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/check
        [HttpPost]
        public HttpResponseMessage Post(PostData data)
        {
            var response = Request.CreateResponse(HttpStatusCode.NotFound, string.Empty);

            if (string.IsNullOrEmpty(data.Content)) return response;

            try
            {
                Regex reg = new Regex(@"((https?|ftp)\://|www.)[A-Za-z0-9\.\-]+(/[A-Za-z0-9\?\&\=;\+!'\(\)\*\-\._~%]*)*", RegexOptions.IgnoreCase);
                List<Uri> list = new List<Uri>(); 

                var matches = reg.Matches(data.Content);
                if (matches.Count == 0)
                {
                    response = Request.CreateResponse(HttpStatusCode.NotFound, matches.Count);
                }
                else
                {
                    if (matches.Count > 1 && data.Version == 1)
                        return response;

                    // ReSharper disable once LoopCanBeConvertedToQuery
                    foreach (Match match in matches)
                    {
                        Uri processedUri = ProcessMatch(match);
                        if (processedUri != null)
                        {
                            list.Add(processedUri);
                        }
                    }

                    if (list.Count > 0)
                    {
                        response = Request.CreateResponse(HttpStatusCode.OK, list.Distinct());
                    }
                    else
                    {
                        Request.CreateResponse(HttpStatusCode.NotFound, string.Empty);
                    }
                }
                
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.ToString());
            }

            return response;
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

        private Uri ProcessMatch(Match match)
        {
            try
            {
                string text = match.ToString();
                if (!text.StartsWith("http"))
                    text = text.Insert(0, "http://");
                Uri url = new Uri(text);

                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var resource in Resources)
                {
                    bool isSitecoreCandidate = IsSitecoreCandidate(url, resource);

                    if (isSitecoreCandidate)
                    {
                        return new Uri(url.GetLeftPart(UriPartial.Authority));
                    }
                }
            }
            catch (Exception)
            {
                //suppress if website is not valid or sitecore, we don't care
            }
            return null;
        }

        private static bool IsSitecoreCandidate(Uri url, Resource resource)
        {
            string webeditCheckLink = url.GetLeftPart(UriPartial.Authority) + resource.Path;
            HttpWebRequest request = WebRequest.Create(webeditCheckLink) as HttpWebRequest;
            //Setting the Request method HEAD, you can also use GET too.
            if (request == null) return false;

            request.Method = "HEAD";
            request.AllowAutoRedirect = true;
            request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            request.CookieContainer = new CookieContainer();
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";

            //Getting the Web Response.
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            //Returns TRUE if the Status code == 200
            if (response != null)
            {
                response.Close();
                if (response.StatusCode == HttpStatusCode.OK && response.ContentType.Contains(resource.Type))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
