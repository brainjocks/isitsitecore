﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IsItSitecore.DTO
{
    public class Resource
    {
        public string Path { get; set; }
        public string Type { get; set; }

        public Resource()
        {
            
        }

        public Resource(string path, string type)
        {
            Path = path;
            Type = type;
        }
    }
}