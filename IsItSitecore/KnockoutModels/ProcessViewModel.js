﻿define(['knockout', 'komapping', 'underscore', 'spin'], function (ko, komapping, _, Spinner) {
	var opts = {
		lines: 13 // The number of lines to draw
	, length: 28 // The length of each line
	, width: 14 // The line thickness
	, radius: 42 // The radius of the inner circle
	, scale: 1 // Scales overall size of the spinner
	, corners: 1 // Corner roundness (0..1)
	, color: '#000' // #rgb or #rrggbb or array of colors
	, opacity: 0.25 // Opacity of the lines
	, rotate: 0 // The rotation offset
	, direction: 1 // 1: clockwise, -1: counterclockwise
	, speed: 1 // Rounds per second
	, trail: 60 // Afterglow percentage
	, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
	, zIndex: 2e9 // The z-index (defaults to 2000000000)
	, className: 'spinner' // The CSS class to assign to the spinner
	, top: '50%' // Top position relative to parent
	, left: '50%' // Left position relative to parent
	, shadow: false // Whether to render a shadow
	, hwaccel: false // Whether to use hardware acceleration
	, position: 'absolute' // Element positioning
	}

	var appTarget = document.getElementById('application-layout');
	var spinner = new Spinner(opts);
	var processViewModel = null;
	ko.mapping = komapping;

	ko.bindingHandlers['class'] = {
		update: function (element, valueAccessor) {
			var currentValue = ko.utils.unwrapObservable(valueAccessor()),
				prevValue = element['__ko__previousClassValue__'],

				// Handles updating adding/removing classes
				addOrRemoveClasses = function (singleValueOrArray, shouldHaveClass) {
					if (Object.prototype.toString.call(singleValueOrArray) === '[object Array]') {
						ko.utils.arrayForEach(singleValueOrArray, function (cssClass) {
							var value = ko.utils.unwrapObservable(cssClass);
							ko.utils.toggleDomNodeCssClass(element, value, shouldHaveClass);
						});
					} else if (singleValueOrArray) {
						ko.utils.toggleDomNodeCssClass(element, singleValueOrArray, shouldHaveClass);
					}
				};

			// Remove old value(s) (preserves any existing CSS classes)
			addOrRemoveClasses(prevValue, false);

			// Set new value(s)
			addOrRemoveClasses(currentValue, true);

			// Store a copy of the current value
			element['__ko__previousClassValue__'] = currentValue.concat();
		}
	};

	ko.bindingHandlers.enterkey = {
		init: function (element, valueAccessor, allBindings, viewModel) {
			var callback = valueAccessor();
			$(element).keypress(function (event) {
				var keyCode = (event.which ? event.which : event.keyCode);
				if (keyCode === 13) {
					callback.call(viewModel);
					return false;
				}
				return true;
			});
		}
	};

	function Check(data) {
		var self = this;

		self.Data = ko.mapping.fromJS(data);
		//validation
		self.Data.Content.extend({ required: true });

		self.submitInProgress = ko.observable(false);
		self.readyToBeChecked = ko.computed(function () {
			return self.Data.Content.isValid() && !self.submitInProgress();
		}, self);

		self.icon = ko.observable("");
		self.overall = ko.observableArray(["", ""]);

		self.checkOne = function () {
			//prevent multiple submit
			self.submitInProgress(true);
			spinner.spin(appTarget);
			//add overlay class
			dim();

			self.Data.Version = 1;
			var dataObject = ko.mapping.toJS(self.Data);

			return $.post('/api/check', dataObject)
				.success(function() {
					self.icon("glyphicon-ok");
					self.overall(["has-feedback", "has-success"]);
				})
				.fail(function() {
					self.icon("glyphicon-remove");
					self.overall(["has-feedback", "has-error"]);
				})
				.always(function () {
					self.submitInProgress(false);
					spinner.stop();
					unDim();
				});;
		}

		self.reset = function () {
			self.Data.Content("");
			self.icon("");
			self.overall(["", ""]);
			self.Data.Content.clearError();
		}
	}

	function MultipleCheck(data) {
		var self = this;

		self.Data = ko.mapping.fromJS(data);
		//validation
		self.Data.Content.extend({ required: true });

		self.submitInProgress = ko.observable(false);
		self.readyToBeChecked = ko.computed(function () {
			return self.Data.Content.isValid() && !self.submitInProgress();
		}, self);
		self.results = ko.observableArray([]);
		self.shouldShowNoResults = ko.computed(function () {
			if (self.results().length > 0) {
				return false;
			} else {
				return true;
			}
		});

		self.checkMultiple = function () {

			dim().promise().done(function () {
				//prevent multiple submit
				self.submitInProgress(true);
				spinner.spin(appTarget);

				self.results([]);

				self.Data.Version = 2;
				var dataObject = ko.mapping.toJS(self.Data);

				return $.post('/api/check', dataObject)
					.success(function (res) {
						self.results(res);
					})
					.fail(function (res) {
						console.log(res);
					})
					.always(function () {

						unDim().promise().done(function () {
							self.submitInProgress(false);
							spinner.stop();
						});;
					});;
			});
		}

		self.reset = function () {
			self.Data.Content("");
			self.results([]);
		}
	}

	function dim() {
		return $("#dim_wrapper").animate({
			'opacity': 0.5,
			'z-index' : 10
		});
	}

	function unDim() {
		return $("#dim_wrapper").animate({
			'opacity': 0.0,
			'z-index': -10
		});
	}

	$.fn.pressEnter = function (fn) {
		return this.each(function () {
			$(this).bind('enterPress', fn);
			$(this).keyup(function (e) {
				if (e.keyCode === 13) {
					$(this).trigger("enterPress");
				}
			});
		});
	};

	// create index view view model which contain two models for partial views
	processViewModel = { checkViewModel: new Check({ Content: "", Version: 1 }), multipleCheckViewModel: new MultipleCheck({ Content: "", Version: 2 }) };
	return processViewModel;
});