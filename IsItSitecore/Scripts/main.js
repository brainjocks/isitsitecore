﻿require.config({
	baseUrl: "/Scripts/",
	paths: {
		knockout: "knockout-3.4.0.debug",
		komapping: "knockout.mapping-latest",
		domready: "domReady",
		jquery: "jquery-2.2.0.min",
		bootstrap: "bootstrap",
		"knockout-amd-helpers": "knockout-amd-helpers",
		text: "text",
		kovalidation: "knockout.validation",
		spin: "spin.min"
	},
	shim: {
		komapping: {
			deps: ['knockout'],
			exports: 'komapping'
		},
		underscore: {
			exports: '_'
		},
		jquery: {
			exports: '$'
		},
		bootstrap: {
			deps: ['jquery']
		}
	}
});

//This function is called when all require scripts are loaded.
//If they are calling define(), then this function is not fired until
//dependencies have loaded, and the arguments will hold the module value for scripts.
require(["jquery", "knockout", "/KnockoutModels/ProcessViewModel.js", "kovalidation", "knockout-amd-helpers", "text", "domready!"],
	function ($, ko, processViewModel) {

		//templating config
		ko.amdTemplateEngine.defaultPath = "/Templates";
		ko.amdTemplateEngine.defaultSuffix = ".template.html";
		ko.amdTemplateEngine.defaultRequireTextPluginName = "text";
		//validation config
		ko.validation.init({
			registerExtenders: true,
			messagesOnModified: true,
			insertMessages: false, //prevent double displaying the error messages
			parseInputAttributes: true,
			messageTemplate: null,
			decorateElement: true,
			errorElementClass: 'err',
			errorClass: 'err'
		}, true);

		// bind view model to referring view
		ko.applyBindings(processViewModel);
});