﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IsItSitecore.ViewModels
{
    public class PostData
    {
        public string Content { get; set; }
        public int Version { get; set; }
    }
}